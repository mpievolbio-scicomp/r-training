---
output:
  pdf_document: default
  html_document: default
---

# R for Beginners - 2 day course

This is a beginners workshop. No previous knowledge of programming or R is required.

You will need a laptop or desktop computer connected to the internet.

Installation of R or RStudio is not required.

# RStudio Server

Please connect to the following link to work with RStudio and R:

<https://rstudio2.evolbio.mpg.de/auth-sign-in?appUri=%2F>

# Timetable

## Day1

| Time  | Title                                             | Lecturer         | Course material |
|----------------|----------------|----------------|-------------------------|
| 10:00 | Welcome and Introduction                          | Kristian Ullrich |                 |
| 10:15 | RStudio and Rmarkdown                             | Kristian Ullrich |                 |
| 10:45 | RStudio - Environment/Console/R Script/R Packages | Kristian Ullrich |                 |
| 11:30 | RMarkdown - Basics                                | Kristian Ullrich |                 |
| 12:00 | Lunch break                                       |                  |                 |
| 13:00 | R Basics                                          | Kristian Ullrich |                 |
| 14:00 | Exercises                                         | Participants     |                 |
| 16:00 | Discussion of Solutions                           | All              |                 |

## Day2

| Time  | Title                                 | Lecturer         | Course material |
|--------------|------------------------------|--------------|--------------|
| 10:00 | Q&A session, recap day1               | Kristian Ullrich |                 |
| 10:30 | Plotting in R                         | Kristian Ullrich |                 |
| 12:00 | Lunch break                           |                  |                 |
| 13:00 | Biostrings in R (nucleotides and AAs) | Kristian Ullrich |                 |
| 14:00 | Exercises                             | Participants     |                 |
| 16:00 | Discussion of Solutions               | All              |                 |

## Use R! book series and other R related books

[A Beginner's Guide to R](https://ebooks.mpdl.mpg.de/ebooks/Record/EB000357124)

[Statistical Analysis of Network Data with R](https://ebooks.mpdl.mpg.de/ebooks/Record/EB000739831)

[Applied Multivariate Statistics with R](https://link.springer.com/book/10.1007%2F978-3-319-14093-3)

[Linear Mixed-Effects Models Using R](https://link.springer.com/book/10.1007%2F978-1-4614-3900-4)

[Nonlinear Regression with R](https://link.springer.com/book/10.1007%2F978-0-387-09616-2)

[Bayesian data analysis in ecology using linear models with R, BUGS, and Stan](https://ebooks.mpdl.mpg.de/ebooks/Record/EB001075973)

[Seamless R and C++ Integration with Rcpp](https://link.springer.com/book/10.1007%2F978-1-4614-6868-4)

[ggplot2](https://link.springer.com/book/10.1007%2F978-3-319-24277-4)

[Learn ggplot2 Using Shiny App](https://link.springer.com/book/10.1007%2F978-3-319-53019-2)

## RStudio Cheatsheets

<https://posit.co/resources/cheatsheets/>

[RStudio](https://rstudio.github.io/cheatsheets/rstudio-ide.pdf)

[base-r](https://rstudio.github.io/cheatsheets/base-r.pdf)

[Rmarkdown](https://rstudio.github.io/cheatsheets/rmarkdown.pdf)

[stringr](https://rstudio.github.io/cheatsheets/strings.pdf)

[purrr](https://rstudio.github.io/cheatsheets/purrr.pdf)

[dplyr](https://rstudio.github.io/cheatsheets/data-transformation.pdf)

[tidyr](https://rstudio.github.io/cheatsheets/tidyr.pdf)

[ggplot2](https://rstudio.github.io/cheatsheets/data-visualization.pdf)

[Regular Expressions](https://rstudio.github.io/cheatsheets/regex.pdf)

[Parallel Computation](https://github.com/rstudio/cheatsheets/blob/main/parallel_computation.pdf)

[github](https://rstudio.github.io/cheatsheets/git-github.pdf)

[keras](https://rstudio.github.io/cheatsheets/keras.pdf)

[reticulate](https://github.com/rstudio/cheatsheets/blob/main/reticulate.pdf)
