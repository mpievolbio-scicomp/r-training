---
output:
  html_document: default
  pdf_document: default
---
# R - Basics

## Content

- Help/Working Directory/Data Types/Data Containers ~ 10 Minutes
- Useful Functions ~ 5 Minutes
- Data Assignment/Data Access/Names/Conditions ~ 30 Minutes
- Data Import/Export ~ 10 Minutes
- Save Data ~ 5 Minutes

## Help/Working Directory/Data Types ~ 10 Minutes

- Getting Help
- Working Directory
- Data Types
- Data Containers

![](sources/BaseR1.png)

![](sources/BaseR2.png)

### Getting Help

Access the help files

```
# Access the help files
?mean
```

Get help of a particular function.

```
help.search('weighted.mean')
```

1. Look at the help page for the `c` function. What kind of vector do you expect will be created if you evaluate the following:

```
c(1, 2, 3)
c("d", "e", "f")
c(1, 2, "f")
```

2. Look at the help for the `paste` function. You will need to use it later. What’s the difference between the `sep` and `collapse` arguments?

### Working Directory

1. Look at the help page for the `setwd` and the `getwd` function.

2. Set the working directory inside the "Files" section.

### Data Types

- Boolean (TRUE, FALSE)
- numeric (1, 0.1, -5)
- character ("a", "1")
- factor

```
typeof(TRUE)
typeof(FALSE)
```

```
typeof(1)
typeof(0.1)
```

```
typeof("a")
typeof("1")
```

```
typeof(factor("a"))
```

### Data Containers

- Vectors
- Lists
- Data Frames
- Matrices
- Arrays

see here for more information

<https://www.statmethods.net/input/datatypes.html>

## Useful Functions ~ 5 Minutes

```
length(object) # number of elements or components

dim(matrix) # dimension of a matrix or data frame

str(object) # structure of an object

class(object) # class or type of an object

names(object) # names

c(object,object,…) # combine objects into a vector

cbind(object, object, …) # combine objects as columns

rbind(object, object, …) # combine objects as rows

object # prints the object

ls() # list current objects

rm(object) # delete an object

newobject <- edit(object) # edit copy and save as newobject

fix(object) # edit in place

rm(list = ls()) # remove all items from current environment

is.na(object) # check if a value is NA

na.omit(object) # returns the object with NA cases removed
```

## Data Assignment/Data Access/Conditions ~ 30 Minutes

### Data Assignment

Data in R is assigned to a any Data Container via the special operator `<-`

```
a <- 1
str(a)
```

```
b <- "string"
str(b)
```

```
c <- "1"
str(c)
```

Comments in R are indicated via the special operator `#` at the beginning of a line

```
# converting a string-number into a number
d <- as.numeric(c)
str(d)
```

### Combine values - Vectors

The Vector Container in R can be either just contain one or multiple values.

```
a <- c(1, 2, 3)
a
```

```
b <- c(4, 5, 6)
b
```

R will automatically distinguish between a vector and known functions like the combine function `c`. Here, a new vector `c` is combined from the vectors `a` and `b`.

```
c <- c(a, b)
c
```

### Overwrite values

To overwrite a vector just assign it again with the same name or just choose a subset to overwrite.

```
c[2] <- 9
c
```

### Create named Vectors

```
pizza_price <- c(pizzasubito=5.64, pizzafresh=6.60, callapizza=4.50)
pizza_price
```

### Data Access from multiple entry vector

To get the full vector named `c`:

```
c
```

To get the first element of the vector `c`:

```
c[1]
```

To get the second element of the vector `c`:

```
c[2]
```

To get the first and third element of the vector `c`:

__Note:__ Here, the second `c` is the combine function!

```
c[c(1, 3)]
```

To get all elements of the vector `c`, but not the first one:

```
c[-1]
```

To get all elements of the vector `c`, but not the first and the third:

```
c[-c(1, 3)]
```

To get elements from a named vector:

```
pizza_price["pizzasubito"]
```

### Combine values into a matrix - column-wise

```
# combine myvector into a matrix column-wise
matrix_by_col <- cbind(a, b)
matrix_by_col
```

### Combine values into a matrix - row-wise

```
# combine myvector into a matrix column-wise
matrix_by_row <- rbind(a, b)
matrix_by_row
```

### Data Access from matrix

To get the full matrix named `matrix_by_col`:

```
matrix_by_col
```

To get the matrix dimension (rows, columns):

```
dim(matrix_by_col)
```

To get third row and second col ():

```
matrix_by_col[3, 2]
```

### Combine different Data Types

The coercion rules go: logical -> integer -> double ("numeric") -> complex -> character, where -> can be read as are transformed into. For example, combining logical and character transforms the result to character:

```
c("a", TRUE)
```

A quick way to recognize `character` vectors is by the quotes that enclose them when they are printed.

### Series of numbers

```
my_series <- seq(from=1, to=10, by=1)
my_series
```

```
my_series <- seq(from=1, to=10, by=0.1)
my_series
```

### Obtain overview of Data

```
# get top two elements
sequence_example <- 20:25
head(sequence_example, n=2)
```

```
# get bottom two elements
sequence_example <- 20:25
tail(sequence_example, n=2)
```

```
# get length of vector
sequence_example <- 20:25
length(sequence_example)
```

### Lists

Another data structure you’ll want in your bag of tricks is the list. A list is simpler in some ways than the other types, because you can put anything you want in it. Remember everything in the vector must be of the same basic Data Type, but a list can have different data types:

```
list_example <- list(1, "a", TRUE, 1+4i)
list_example
```

```
str(list_example)
```

What is the use of lists? They can organize data of different types. For example, you can organize different tables that belong together, similar to spreadsheets in Excel. But there are many other uses, too.

We will see another example that will maybe surprise you in the next chapter.

To retrieve one of the elements of a list, use the double bracket:

```
list_example[[2]]
```

The elements of lists also can have names, they can be given by prepending them to the values, separated by an equals sign:

```
another_list <- list(title = "Numbers", numbers = 1:10, data = TRUE )
another_list
```

This results in a named list. Now we have a new function of our object! We can access single elements by an additional way!

```
another_list$title
```

### Data Frames

A `data.frame` is really a list of vectors. It is a special list in which all the vectors must have the same length. The different list can have different Data Types.

```
data("iris")
str(iris)
```

```
iris
```

```
class(iris)
```

```
iris$Sepal.Length
```

Data Access from `data.frame` object:

```
cats[1]
cats[[1]]
cats$coat
cats["coat"]
cats[1, 1]
cats[, 1]
cats[1, ]
```

Recently many R packages rely on the package tidyverse, it use a different extended data.frame class called tibble.

One important change is that the data will just show a subset, which is nice if the `data.frame` would be large and would print all over the screen.

```
library(tidyverse)
iris_tibble <- as_tibble(iris)
iris_tibble
```

#### Matrices

As already seen, all columns in a matrix must have the same mode(numeric, character, etc.) and the same length.

```
# generates 5 x 4 numeric matrix
y<-matrix(1:20, nrow = 5,ncol = 4)
y
```

```
# another example
cells <- c(1, 26, 24, 68, 12, 35)
rnames <- c("R1", "R2", "R3")
cnames <- c("C1", "C2", "C3")
mymatrix <- matrix(cells, nrow = 3, ncol = 3, byrow = TRUE,
  dimnames=list(rnames, cnames))
mymatrix
```

Identify rows, columns or elements using subscripts:

```
mymatrix[, 2] # 2nd column of matrix
```

```
mymatrix[2,] # 2nd row of matrix
```

```
mymatrix[2:3, 1:2] # rows 2,3 of columns 1,2
```

Remove one row or column:

```
mymatrix[, -1]
```

```
mymatrix[-1 ,]
```

#### Arrays

Arrays are similar to matrices but can have more than two dimensions. See help(array) for details.

```
?array
```

#### Names

With names, we can give meaning to elements. It is the first time that we do not only have the data, but also explaining information. It is metadata that can be stuck to the object like a label. In R, this is called an attribute. Some attributes enable us to do more with our object, for example, like here, accessing an element by a self-defined name.

```
a <- c(1, 2, 3)
names(a) <- c("A", "B", "C")
```

```
a["C"]
```

#### Missing Data

Not all datasets are complete and one needs to deal with missing data.

This missing data might be encoded in your data by different charchters (“NA” or “NaN” or “-”) or numbers (9999, -1, -999).

During data import one can directly specify missing data to be correctly interpreted and set to NA in R.

```
my.na.vector <- c(1, 2, 99, 3, 4)
my.na.vector
my.na.vector[my.na.vector == 99] <- NA
my.na.vector
```

__Note:__ Arithmetic functions on missing values yield missing values.

```
mean(my.na.vector)
```

Some functions have a pre-build missing value handling.

```
mean(my.na.vector, na.rm = TRUE)
```

#### Conditions

<https://www.statmethods.net/management/operators.html>

```
10 > 5
5 != 5
#
a <- 1
a == 1
#
b <- c(1, 2, NA)
2 %in% b
# 
is.na(1)
is.na(b[3])
!is.na(b[3])
```

## Data Import/Export ~ 10 Minutes

### Data Import

Data can be imported via the "Import Dataset" button or via the function `read.table` or `readLines`.

The `readLines` function will read a text file line by line, the `read.table` function will try to directly convert the data based on a delimiter into a matrix object.

The `readr` library offers multiple function to read text based table like data:

- `read_csv`
- `read_tsv`

One can select the delimiter or can specify if a header line exist in the text file.

As an example we will first write the `iris` Dataset into a text file to import it again via the mentioned functions. The individual `write.table` options will be explained separately.

```
data("iris")
write.table(iris, file="iris.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
```

To import the text file, one can use the default R function `read.table`:

```
my_data <- read.table(file="iris.tsv", sep="\t", header=TRUE)
str(my_data)
```

```
my_data
```

Now we will use the `read_tsv` function from the `readr` library:

```
my_data2 <- read_tsv(file="iris.tsv")
```

### Data Export

As used before, one can use the `write.table` function to write the data into a text based file.

__Note:__ Be careful to choose the correct and anticipated `col.names`, `row.names`, `sep` and `quote` output option.

One can also use the `write_tsv` function from the `readr` library.

## Save Data ~ 5 Minutes

R objects can be directly save with the `save` function and loaded with the `load` function.

Here, we will save the `iris` Dataset as it is into a file and load it again.

### Save R object

```
data("iris")
save(iris, file="iris.RData")
```

### Load R object

__Note:__ The name of the object will stay the same.

```
load(file="iris.RData")
```

If you want to save R objects and load it using a different name, use the `saveRDS` and `readRDS` functions.

### Save R object as RDS

```
saveRDS(iris, file="iris.rds")
```

### Load R object from RDS

```
iris2 <- readRDS(file="iris.rds")
```
