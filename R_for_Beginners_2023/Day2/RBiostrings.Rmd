---
output:
  pdf_document: default
  html_document: default
---
# R - Biostrings

Memory efficient string containers, string matching algorithms, and other utilities, for fast manipulation of large biological sequences or sets of sequences.

## Content

- XString class ~ 15 Minutes
- Base content ~ 5 Minutes
- Consensus ~ 5 Minutes
- Pairwise Sequence Alignment ~ 15 Minutes

## XString class ~ 10 Minutes

- DNAString
- RNAString
- AAString
- Codons
- DNAStringSet

### DNAString

```{r}
library(Biostrings)
dna <- DNAString("TTGAAAA-CTC-N")
dna
```

```{r}
length(dna)
```

```{r}
dna[3]
```

```{r}
dna[7:12]
```

Substrings can be extracted.

```{r}
subseq(dna, start=7, end=12)
```

To dump an XString object as a character vector (of length 1), use the toString method:

```{r}
toString(dna)
```

```{r}
IUPAC_CODE_MAP
```

### RNAString

When comparing an RNAString object with a DNAString object, U and T are considered
equals:

```{r}
rna <- RNAString(dna)
dna == rna
```

```{r}
Views(dna, start=3:0, end=5:8)
```

### AAString

```{r}
AA_ALPHABET
```

Translate DNA >>> AA

```{r}
dna <- DNAString("ATGCTAATA")
aa <- translate(dna)
aa
```

Translate DNA >>> AA using a different genetic code:

```{r}
GENETIC_CODE_TABLE
```

```{r}
dna <- DNAString("ATGCTAATA")
SGC1 <- getGeneticCode("SGC1")
SGC1
aa <- translate(dna, genetic.code=SGC1)
aa
```

Possible errors:

#### not multiple of three

```{r}
dna <- DNAString("ATGCTAAT")
translate(dna)
```

```{r}
dna <- DNAString("ATGCTAAT")
translate(dna[1:6])
```

#### Fuzzy codon:

```
dna <- DNAString("ATGCTANNA")
translate(dna)
```

```{r}
dna <- DNAString("ATGCTANNA")
translate(dna, if.fuzzy.codon="X")
```

### Codons

```{r}
dna <- DNAString("ATGCTAATA")
codons(dna)
```

### DNAStringSet

```{r}
dnaset <- DNAStringSet(c("ATGCTAATA", "ATGTGT", "ATGCTAATT"))
dnaset
```

```{r}
translate(dnaset)
```

## Base content ~ 5 Minutes

- Base content

```{r}
library(ape)
data(woodmouse)
dna <- woodmouse |> as.character() |> apply(1, paste0, collapse="") |> DNAStringSet()
```

```{r}
alphabetFrequency(dna)
```

## Consensus ~ 5 Minutes

- Consensus Matrix
- Consensus Sequence

```{r}
dna_consensus <- consensusMatrix(dna)
dim(dna_consensus)
```

```{r}
dna_consensus[, 1:6]
```

```{r}
?consensusString
```


```{r}
consensusString(dna)
```

## Pairwise Sequence Alignment

- Global Alignment
- Local Alignment

### Global Alignment

```{r}
data(BLOSUM62)
BLOSUM62
```

```{r}
?pairwiseAlignment
```

```{r}
pairwiseAlignment(AAString("PAWHEAE"), AAString("HEAGAWGHEE"),
                  type="global",
                  substitutionMatrix=BLOSUM62,
                  gapOpening=12,
                  gapExtension=4)
```

### Local Alignment

```{r}
pairwiseAlignment(AAString("PAWHEAE"), AAString("HEAGAWGHEE"),
                  type="local",
                  substitutionMatrix=BLOSUM62,
                  gapOpening=12,
                  gapExtension=4)
```
