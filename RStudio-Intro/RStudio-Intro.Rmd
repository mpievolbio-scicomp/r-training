---
title: "RStudio/RMarkdown - Introduction"
author: "Kristian Ullrich"
date: "5/05/2020"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Timeline

11.00 - 12.00 : Rstudio/RMarkdown Introduction

13.30 - 16.00 : R types, R classes, R functions, R packages, ...

## RStudio - Intro

RStudio is an integrated development environment (IDE) for R.

![RStudio](sources/RStudioLogo.png){width=100px}
<https://rstudio.com/>

RStudio has become a rich development tool extending constantly their protofolio of tools and apps related to R.

![RStudioTimeline](sources/RStudioTimeline.png){width=400px}

![RStudioPortofolio](sources/RStudioPortofolio.png){width=400px}

RStudio comes pre-compiled for any Operating System (OS).

![RStudioDownloads](sources/RStudioDownloads.png){width=400px}

## RStudio - IDE

### RStudio - Appearence

![RStudioAppearence](sources/RStudioAppearence.png){width=400px}

### RStudio - Environment/History/Connections

The Environment highlights the R-packages that are currently loaded. If one clicks on a package one can see all its functions that live inside this package.

One can also look into the current loaded packages via the **Console**.

```{r}
sessionInfo()
```

![RStudioEnvironment](sources/RStudioEnvironment.png){width=400px}

Via the **Import Dataset** button it is possible to import external data (will be covered later on in this R course).

![RStudioImportDataset](sources/RStudioImportDataset.png){width=400px}

The history tab lists all commands that has been typed into the **Console**.

Via the Connections tab one could connect to a database (not covered in this R course).

![RStudioConnections](sources/RStudioConnections.png){width=400px}

## R Markdown

During this R course you will use the **Markdown** language to write and fill up your own documentation of this R course.

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. 

### Create a new Markdown file within RStudio

![NewMarkdownFile](sources/NewMarkdownFile.png){width=400px}

### R Markdown File Types

There are differnet R Markdown Types that one can choose to create.

This R course will use a simple HTML Document.

However, once you have time after the R course you should check out the other useful examples.

![RMarkdownDocument](sources/RMarkdownDocument.png){width=400px}

![RMarkdownPresentation](sources/RMarkdownPresentation.png){width=400px}

![RMarkdownShiny](sources/RMarkdownShiny.png){width=400px}

![RMarkdownTemplate](sources/RMarkdownTemplate.png){width=400px}

### Markdown Basics

See here for more information about Markdown:

<https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html>

R Markdown Cheatsheet:

<https://www.rstudio.com/wp-content/uploads/2016/03/rmarkdown-cheatsheet-2.0.pdf>

#### Headings

A level-one heading
===================
 
A level-two heading
-------------------

#### ATX-style Headings

# A level-one heading

## A level-two heading

### List

1. First element
2. Second element
- Bullet point 1
- Bullet point 2

### Bolad and Italic

**bold**

*italic*

### Mathmatics in Markdown

> Example:
>
> $\sum_{n=1}^{10} n^2$
>
> $\displaystyle \frac{a}{b}$
>
> $\delta \Delta$

see here for more examples

<https://www.calvin.edu/~rpruim/courses/s341/S17/from-class/MathinRmd.html>

### Include images

```
![RStudioLogo](sources/RStudioLogo.png){width=100px}
```
![RStudioLogo](sources/RStudioLogo.png){width=100px}
```
![RStudioLogo](sources/RStudioLogo.png){width=200px}
```
![RStudioLogo](sources/RStudioLogo.png){width=200px}


#### Block quotations

> This is a block quote.
>
> This paragraph has two lines.
>
> 1. This is a list inside a block quote.
> 2. Second item.

## Including R code

You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including R Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

## Including page break

```
\pagebreak 
```

\pagebreak 

## Including R Table data

You can produce nice tables like this:
```{r}
library(knitr)
kable(mtcars[1:5,], caption = "Table1: Table caption")
```
