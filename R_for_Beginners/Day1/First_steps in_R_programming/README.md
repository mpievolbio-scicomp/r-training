Get to know basic R types and R related environment functions as given here:

<https://www.statmethods.net/>

<https://raw.githubusercontent.com/rstudio/cheatsheets/master/base-r.pdf>
