Basically in this part, the participants will get to know and how to use RStudio.

<https://rstudio.gwdg.de/>

How to create a RMarkdown file (*.Rmd).

Get to know the basic syntax of a RMarkdown file as given here:

<https://raw.githubusercontent.com/rstudio/cheatsheets/master/rmarkdown-2.0.pdf>

A good RMarkdown guide

<https://bookdown.org/yihui/rmarkdown/>