# R for Beginners - 2 day course

This is a beginners workshop. No previous knowledge of programming or R is required.

You will need a laptop or desktop computer connected to the internet. 

Installation of R or RStudio is not required.

# RStudio Server

Please connect to the following link to work with RStudio and R:

https://rstudio.gwdg.de/

# Timetable

## Day1

| Time  | Title | Lecturer | Course material |
| ----- | ----- | -------- | --------------- |
| 10:00 | Welcome and Introduction | Loukas Theodosiou | |
| 10:10 | RStudio and Rmarkdown | Kristian Ullrich |  https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/blob/master/R_for_Beginners/Presentations/RStudio_and_Rmarkdown/RStudio_and_RMarkdown.pdf |
| 10:45 | RStudio and Rmarkdown - Hands On | Kristian Ullrich | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day1/RStudio_and_Rmarkdown |
| 11:30 | Version control with RStudio | Carsten Fortmann-Grote | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day1/Version_control_with_RStudio |
| 12:00 | Lunch break | | |
| 13:00 | First steps in R programming | Kristian Ullrich | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/blob/master/R_for_Beginners/Presentations/First_steps_in_R_programming/First_steps_in_R_programming.pdf |
| 13:15 | First steps in R programming - Hands On | Kristian Ullrich | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day1/First_steps%20in_R_programming |
| 14:00 | Exercises | Participants | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day1/Exercises |
| 16:00 | Discussion of Solutions | All | |

## Day2

| Time  | Title | Lecturer | Course material |
| ----- | ----- | -------- | --------------- |
| 10:00 | Q&A session, recap day1| Loukas Theodosiou, Kristian Ullrich, Carsten Fortmann-Grote | |
| 10:30 | Plotting with ggplot part1 | Loukas Theodosiou | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day2/Plotting_with_ggplot |
| 12:00 | Lunch break | | |
| 13:00 | Plotting with ggplot part2 | Loukas Theodosiou | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day2/Plotting_with_ggplot |
| 14:00 | Exercises | Participants | https://gitlab.gwdg.de/mpievolbio-scicomp/r-training/-/tree/master/R_for_Beginners/Day2/Exercises |
| 16:00 | Discussion of Solutions | All | |

## Use R! book series and other R related books

[A Beginner's Guide to R](https://ebooks.mpdl.mpg.de/ebooks/Record/EB000357124)

[Statistical Analysis of Network Data with R](https://ebooks.mpdl.mpg.de/ebooks/Record/EB000739831)

[Applied Multivariate Statistics with R](https://link.springer.com/book/10.1007%2F978-3-319-14093-3)

[Linear Mixed-Effects Models Using R](https://link.springer.com/book/10.1007%2F978-1-4614-3900-4)

[Nonlinear Regression with R](https://link.springer.com/book/10.1007%2F978-0-387-09616-2)

[Bayesian data analysis in ecology using linear models with R, BUGS, and Stan](https://ebooks.mpdl.mpg.de/ebooks/Record/EB001075973)

[Seamless R and C++ Integration with Rcpp](https://link.springer.com/book/10.1007%2F978-1-4614-6868-4)

[ggplot2](https://link.springer.com/book/10.1007%2F978-3-319-24277-4)

[Learn ggplot2 Using Shiny App](https://link.springer.com/book/10.1007%2F978-3-319-53019-2)

## RStudio Cheatsheets

https://www.rstudio.com/resources/cheatsheets/

[base-r](http://github.com/rstudio/cheatsheets/raw/master/base-r.pdf)

[advancedR](https://www.rstudio.com/wp-content/uploads/2016/02/advancedR.pdf)

[Rmarkdown](https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf)

[Rmarkdown reference Guide](https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf)

[stringr](https://github.com/rstudio/cheatsheets/raw/master/strings.pdf)

[purrr](https://github.com/rstudio/cheatsheets/raw/master/purrr.pdf)

[dplyr](https://github.com/rstudio/cheatsheets/raw/master/data-transformation.pdf)

[ggplot2](https://github.com/rstudio/cheatsheets/raw/master/data-visualization-2.1.pdf)

[Regular Expressions](https://github.com/rstudio/cheatsheets/raw/master/regex.pdf)

[Parallel Computation](https://github.com/rstudio/cheatsheets/raw/master/parallel_computation.pdf)

[keras](https://github.com/rstudio/cheatsheets/raw/master/keras.pdf)

