#Maximum Likelihood 
#https://www.r-bloggers.com/2019/08/maximum-likelihood-estimation-from-scratch/
#In other words, it calculates the random population that is most likely to generate the observed data, 
#while being constrained to a particular type of distribution.
#Instead, the MLE method is generally applied using algorithms 
#known as non-linear optimizers. You can feed these algorithms any 
#function that takes numbers as inputs and returns a number as ouput and
#they will calculate the input values that minimize or maximize the output. 

set.seed(2019)
sample = rnorm(100)
prod(dnorm(sample))
## [1] 2.23626e-58

NLL = function(pars, data) {
  # Extract parameters from the vector
  mu = pars[1]
  sigma = pars[2]
  # Calculate Negative Log-LIkelihood
  -sum(dnorm(x = data, mean = mu, sd = sigma, log = TRUE))
}

mle = optim(par = c(mu = 0.2, sigma = 1.5), fn = NLL, data = sample,
            control = list(parscale = c(mu = 0.2, sigma = 1.5)))
mle$par


data = data.frame(t = c(0, 16, 22, 29, 36, 58),
                  G = c(0, 0.12, 0.32, 0.6, 0.79, 1))
plot(data, las = 1, xlab = "Days after sowing", ylab = "Ground cover")

G = function(pars, t) {
  # Extract parameters of the model
  Gmax = pars[1]
  k = pars[2]
  th = pars[3]
  # Prediction of the model
  DG = Gmax/(1 - 1/(1 + exp(k*th)))
  Go = DG/(1 + exp(k*th))
  DG/(1 + exp(-k*(t - th))) - Go
}

NLL = function(pars, data) {
  # Values predicted by the model
  Gpred = G(pars, data$t)
  # Negative log-likelihood 
  -sum(dnorm(x = data$G, mean = Gpred, sd = pars[4], log = TRUE))
}


par0 = c(Gmax = 1.0, k = 0.15, th = 30, sd = 0.01)
fit = optim(par = par0, fn = NLL, data = data, control = list(parscale = abs(par0)), 
            hessian = TRUE)
fit$par

plot(data, las = 1, xlab = "Days after sowing", ylab = "Ground cover")
curve(G(fit$par, x), 0, 60, add = TRUE)

#https://www.r-bloggers.com/2020/07/maximum-likelihood-estimation/

n_samples <- 25; true_rate <- 1; set.seed(1)
exp_samples <- rexp(n = n_samples,
                    rate = true_rate)


library(fitdistrplus)
sample_data <- exp_samples
rate_fit_R <- fitdist(data = sample_data, 
                      distr = 'exp', 
                      method = 'mle')


#http://web.stanford.edu/class/earthsys214/notes/fit.html
#That seems even better 

fit.logistic <- function(par,y){
  r <- par[1]; k <- par[2]
  t <- y[,2]
  n <- y[,1]
  n0 <- y[1]
  tmp <- n[1] *exp(r*t)/(1 + n[1] * (exp(r*t)-1)/k)
  sumsq <- sum((n - tmp)^2)
}

usa <- c(3.929214,   5.308483,   7.239881,   9.638453,  12.866020,  
         17.866020, 23.191876,  31.443321,  38.558371,  50.189209,
         62.979766,  76.212168, 92.228496, 106.021537, 123.202624,
         132.164569, 151.325798, 179.323175, 203.302031, 226.542199,
         248.709873, 281.421906, 308.745538)
year <- seq(1790,2010,10) # decennial census
r.guess <- (log(usa[15])-log(usa[1]))/140
k.guess <- usa[15] #1930 US population
par <- c(r.guess,k.guess)

census1930 <- cbind(usa[1:15], seq(0,140,by=10))
usa1930.fit <- optim(par,fit.logistic,y=census1930)
usa1930.fit$par

logistic.int <- expression(n0 * exp(r*t)/(1 + n0 * (exp(r*t) - 1)/k))
r <- usa1930.fit$par[1]
k <- usa1930.fit$par[2]
n0 <- usa[1]
t <- seq(0,220,by=10)
plot(seq(1790,2010,by=10), usa, type="n", xlab="Year", 
     ylab="Total Population Size (Millions)")
lines(seq(1790,2010,by=10), eval(logistic.int), lwd=2, col=grey(0.85))
points(seq(1790,2010,by=10),usa, pch=16, col="black")
abline(v=1930, lty=3, col="red")


plague <- read.table("https://web.stanford.edu/class/earthsys214/data/us.plague.txt", header=TRUE)
names(plague)
plot(plague, type="l", xlab="Year", ylab="Plague Cases")

#assumption 1 We can calculate the maximum likelihood estimator of the distribution of plague cases 
#in the US. To do this, we will make the rather large assumption that the number of cases 
#is independent of previous years.

#n order to calculate the MLE, we need to write a function that calculates the negative-log-likelihood.
#his function takes two arguments, the data x and the model parameter lambda. Because the Poisson distribution
#has only one parameter, the optimization routine that we use in R is optimize(). 
#The arguments that optimize() requires are the function which calculates the negative-log-likelihood, a range over which to search for the minimum, and the data to use in our likelihood function.
#in every case they seem to be doing two stuff one is to write the likelihood function and the 
#other is to minimize the likelihood fucntion


# In statistics, maximum likelihood estimation (MLE) is a method 
# of estimating the parameters of a statistical model given observations, 
# by finding the parameter values that maximize the likelihood of making the observations 
# given the parameters.

# n Bayesian statistical inference, 
# a prior probability distribution, often simply called the prior,
# of an uncertain quantity is the probability distribution 
# that would express one's beliefs about this quantity before some evidence is taken into account.