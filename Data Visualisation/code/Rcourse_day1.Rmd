---
title: "R_Course"
author: "Loukas Theodosiou, PhD"
date: "4/28/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
```

## PLot the data 
### make a box plot of the iris data to see how the Sepal.Length is distributed across species
### Here we plot one catgorical variable with one continuous variable?
### What other plot we could do?
```{r}
library(ggplot2)
ggplot(iris, aes(x=Species, y=Sepal.Length)) + geom_boxplot() 
```
```{r}
ggplot(iris, aes(x=Species, y=Sepal.Length)) + geom_boxplot() + geom_text(x="setosa",y=7, label="I hate grey background")
```

### it might be more cool to be able to see the data points on the background 

```{r}
ggplot(iris, aes(x=Species, y=Sepal.Length)) + geom_boxplot() + geom_point()
```
### the points do not look good. All of them are in the same line. I want to them to be spread in space 

```{r}
ggplot(iris, aes(x=Species, y=Sepal.Length)) + geom_boxplot() + geom_jitter()
```

help(geom_jitter) 
# In addition I would like to specify the area in which the points will spread and make my plot 
# fully reproducible
```{r}
# here there are two ways to make the plot reproducible
set.seed(1);
ggplot(iris, aes(x=Species, y=Sepal.Length)) + geom_boxplot() + geom_jitter(width = 0.25,height=0.25)
```

## How to choose colors: 
1. There is the conventional way where you can choose 
http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf

2. There are more advanced way like colorbrewer2 
https://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3
and adobe color palletes. https://color.adobe.com/explore
Always thing of the color blind people

3.https://paletton.com/#uid=1240u0kcmuL3RQc7NEKhspGmLkj

4.https://www.sessions.edu/color-calculator/

5. color wheel of Newton http://www.webexhibits.org/colorart/bh.html
 Isaac Newton’s 1666 color wheel which shows the relationship between colors. Sir Isaac Newton created the color wheel based on his experiments with prisms that led to the theory that red, yellow and blue were the primary colors from which all other colors are derived.
 https://www.hueandtonecreative.com/blog/2019/9/9/how-to-improve-your-designs-using-color-theory
 
6.https://github.com/karthik/wesanderson Download palletes or make your own palletes
 
```{r}
set.seed(1);
ggplot(iris, aes(x=Species, y=Sepal.Length, color=Species)) + geom_boxplot(fill=NA) +
  geom_jitter(width = 0.25,height=0.25) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
  theme_minimal()
```

### How can I choose different themes? https://www.datanovia.com/en/blog/ggplot-themes-gallery/


```{r}
set.seed(1);
ggplot(iris, aes(x=Species, y=Sepal.Length, color=Species)) + geom_boxplot(fill=NA) +
  geom_jitter(width = 0.25,height=0.25) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
  theme_minimal() +
  labs(title="Sepal length over different species")
```
### Customise theme further https://ggplot2.tidyverse.org/reference/theme.html It is crazy of what you can do. when you want to build your theme you have the following elements you can apply.
element_line(), element_text(),element_rect(),element_blank()



```{r}
set.seed(1);
ggplot(iris, aes(x=Species, y=Sepal.Length, color=Species)) + geom_boxplot(fill=NA) +
  geom_jitter(width = 0.25,height=0.25) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
    theme_minimal() +
  theme(plot.title = element_text(size =14,hjust=0.5,face = "bold", family="Avenir"),
                legend.title.align=0.5,
        panel.grid.major  = element_line(colour = "#F0F0F2", size=0.5),#if you put color=NA or color=red then you can show them the effect of panel.grid.major and panel.grid.minor
        axis.ticks.x = element_line(colour = "#333333"),
        axis.ticks.y = element_line(colour = "#333333"),
        axis.ticks.length =  unit(0.26, "cm"),
        panel.border = element_rect(colour="#333333", fill=NA))+
  labs(title="Sepal length over different species") 
```

#change the order of the elements in the x-axis.
```{r}
head(iris)
str(iris)
iris$Species <- factor(iris$Species, levels(unique(iris$Species)))
iris$Species <- factor(iris$Species, levels=c("versicolor", "setosa", "virginica"))

set.seed(1);
ggplot(iris, aes(x=Species, y=Sepal.Length, color=Species)) + geom_boxplot(fill=NA) +
  geom_jitter(width = 0.25,height=0.25) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
    theme_minimal() +
  theme(plot.title = element_text(size =14,hjust=0.5,face = "bold", family="Avenir"),
                legend.title.align=0.5,
        panel.grid.major  = element_line(colour = "#F0F0F2", size=0.5),#if you put color=NA or color=red then you can show them the effect of panel.grid.major and panel.grid.minor
        axis.ticks.x = element_line(colour = "#333333"),
        axis.ticks.y = element_line(colour = "#333333"),
        axis.ticks.length =  unit(0.26, "cm"),
        panel.border = element_rect(colour="#333333", fill=NA))+
  labs(title="Sepal length over different species") 

```


### lets go crazy
```{r}
set.seed(1);
ggplot(iris, aes(x=Species, y=Sepal.Length, color=Species)) + geom_boxplot(fill=NA) +
  geom_jitter(width = 0.25,height=0.25) +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
    theme_minimal() +
  theme(plot.title = element_text(size =14,hjust=0.5,face = "bold", family="Avenir"),
                legend.title.align=0.5, legend.position = "bottom",
        panel.grid.major  = element_line(colour = "#F0F0F2", size=0.5),#if you put color=NA or color=red then you can show them the effect of panel.grid.major and panel.grid.minor
        axis.ticks.x = element_line(colour = "#333333"),
        axis.ticks.y = element_line(colour = "#333333"),
        axis.ticks.length =  unit(0.26, "cm"),
        #axis.text.x = element_text(angle=90, hjust=1), 
        axis.text.x = element_text(margin = margin(t = .3, unit = "cm"),size=12,color="black"),
        axis.title.x = element_text(margin = margin(t = .3, unit = "cm"),color = "black", size = 12, angle = 0, hjust = .5, vjust = 0, face = "italic"),

        panel.border = element_rect(colour="#333333", fill=NA),
        )+
  labs(title="Sepal length over different species") 


```

# Make a raincloud plot. This is supposed to be quite a lot in fashion recently.
https://rdrr.io/cran/chronicle/man/make_raincloud.html

rm(list=ls())
```{r}
head(iris)
install.packages("chronicle")
library(chronicle)

make_raincloud(dt=iris,
               value="Sepal.Length",
               groups = "Species",
               static = TRUE, adjust = 1,
               plot_palette= c("#00AFBB", "#E7B800", "#FC4E07")) 
               #plot_palette_generator = "plasma")

```






# Data Preparation for doing more plots
### count observations, summarise, summarise_all, summarise_at, summarise_if

```{r}
iris #one of the most famous data sets
library(tidyverse)
#check for differences among species
head(iris)

iris %>% 
  count(Species,sort=TRUE)

iris %>% 
  group_by(Species) %>% #group by Species
  add_tally()  #add a column with the number of observations

iris %>% 
  group_by(Species) %>% #group by Species
  mutate(observations=n())  # this is the same as add_tally()

iris %>% 
  select(Sepal.Length) %>% 
  add_count()

```
```{r}
library(dplyr)

iris %>% 
  group_by(Species) %>% 
  summarise_all(list(mean= ~mean(.), sd= ~sd(.), se = ~sd(./sqrt(.))), na.rm=TRUE)

#the "~" symbol introduces a function and "." symbol introduces the column names in each group
# insteal of the "~" you can use the "funs"

iris %>% 
  group_by(Species) %>% 
  summarise_all(funs(mean= mean(.), sd=sd(.), se =sd(./sqrt(.))), na.rm=TRUE) %>% 
  arrange(desc(Sepal.Length_mean))


##summarise_at first it needs information about the columns you want to consider. In this case 
## you need to wrap them insite a vars() statement. 
# After you select the columns with vars() you need to tell them what function he should apply/

iris2=iris %>% 
  group_by(Species) %>% 
  summarise_at(vars(Sepal.Length, Sepal.Width), list(mean= ~mean(.), sd=~sd(.), se=~sd(./sqrt(.))), na.rm=TRUE)

iris %>% 
  group_by(Species) %>% 
  summarise_if(is.numeric, list(mean= ~mean(.), sd=~sd(.), se=~sd(./sqrt(.))), na.rm=TRUE)

```
### Here we want to plot the mean with standard error
```{r}
ggplot(iris2, aes(x=Species, Sepal.Length_mean, color=Species)) + geom_point() +theme_bw() +
geom_errorbar(aes(ymin=Sepal.Length_mean - Sepal.Length_se, ymax=Sepal.Length_mean +Sepal.Length_se), width=0.1) + 
scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))
#geom_errorbarh(data = z,aes(xmin = Peak - PeakSE,xmax = Peak + PeakSE,y = Rise,colour = Group,height = 0.01)) + 

```


### Plot individual observation and plot means in ggplot2

head(iris)
head(iris2)
colnames(iris2)

```{r}
set.seed(1);
ggplot(iris2, aes(x=Species, Sepal.Length_mean, color=Species)) + geom_point() +theme_bw() +
geom_errorbar(aes(ymin=Sepal.Length_mean - Sepal.Length_se, ymax=Sepal.Length_mean +Sepal.Length_se), width=0.1) + 
scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07")) +
  geom_jitter(data=iris, aes(x=Species, y=Sepal.Length,color=Species), alpha=0.5,size=2,shape=16, stroke=0)

#you can use shapes with outline and for this you need to choose shape=21
set.seed(1);
ggplot(iris2, aes(x=Species, Sepal.Length_mean, color=Species)) + geom_point() +theme_bw() +
geom_errorbar(aes(ymin=Sepal.Length_mean - Sepal.Length_se, ymax=Sepal.Length_mean +Sepal.Length_se), width=0.1) + 
scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07")) +
  geom_jitter(data=iris, aes(x=Species, y=Sepal.Length),)

```



### A fast alternative can be the ggpubr packcage https://cran.r-project.org/web/packages/ggpubr/ggpubr.pdf

```{r}
library("ggpubr")
library(reshape2)

iris3=melt(iris,id.vars = "Species")
head(iris3)
iris4=subset(iris3,variable=="Sepal.Length")
nrow(iris4)

my_comparisons <- list(c("setosa", "versicolor"), c("setosa", "virginica"), c("versicolor", "virginica"))
ggerrorplot(iris4, x = "Species", y = "value", 
            desc_stat = "mean_sd",color = "Species", palette = get_palette(c("#00AFBB", "#E7B800", "#FC4E07"),3),
            #add.params = list(color = "darkgray"), #that gives a color to the data points
            #add = "dotplot", dot plot also bins the data, stat_bindot()
            add = "jitter")+
            stat_compare_means(comparisons = my_comparisons)  # Add pairwise comparisons p-value
  #stat_compare_means(label.y = 50)                  # Add global p

#compare_means(len ~ dose,  data = ToothGrowth, ref.group = ".all.",
              #method = "t.test")

my_comparisons <- list(c("setosa", "versicolor"), c("setosa", "virginica"), c("versicolor", "virginica"))
ggerrorplot(iris4, x = "Species", y = "value", 
            desc_stat = "mean_sd",color = "Species", add="jitter") +
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))
                         

```




### Are they significanlty different the means of these three samples 
```{r}
sepal.length.aov=aov(Sepal.Length ~ Species, data=iris)
TukeyHSD(sepal.length.aov)
```
### The difference in test scores between versicolor and setosa is 0.930, 
with versicolors averaging 0.930 points higher.
### The 95% confidence interval of that difference is between -0.6862273  and 1.1737727 points.

### Now I would like to see the relatioship between the Sepal.Length and Petal.Length which are both continuous variables

library(scales)
```{r}
ggplot(iris, aes(Sepal.Length,Petal.Length, color=Species)) + geom_point(size=3.5,shape=16,alpha=0.5) + theme_bw()+
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07")) +
  scale_x_continuous(breaks = seq(4,8,0.5), limits=c(4,8)) +
  labs(title="Sepal ~ Petal over species",
       caption = "class 2020") +
  geom_smooth(aes(fill=Species),method="lm", se=TRUE, alpha=0.5) +
  scale_fill_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))

```

### If a linear form does not work we can always refer to other polynomial function such as the 
 a quadratic (one bend), or cubic (two bends) line is used. It is rarely necessary to use a higher order( >3 ) polynomials. 

### Which are the other methods that I could use with geom_smooth 
https://ggplot2.tidyverse.org/reference/geom_smooth.html



```{r}
library(ggpubr)
ggplot(iris, aes(Sepal.Length,Petal.Length, color=Species)) + geom_point(size=3.5,shape=16,alpha=0.5) + theme_bw()+
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07")) +
  scale_x_continuous(breaks = seq(4,8,0.5), limits=c(4,8)) +
  labs(title="Sepal ~ Petal over species",
       caption = "class 2020") +
  geom_smooth(aes(fill=Species),method="lm", se=TRUE, alpha=0.5) +
  scale_fill_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
  stat_cor(method="pearson")
```



```{r}
library(ggradar)
library(scales)
suppressMessages(library(dplyr))

iirisdata <- iris %>%
            group_by( Species ) %>%
            mutate_each(funs(rescale)) %>%
            summarise(mean(Sepal.Length), mean(Sepal.Width), mean( Petal.Length ), mean( Petal.Width))
irisdata
iris2
ggradar( irisdata )
```






```{r}
library(ggpubr)
head(iris2)

ggplot(iris2, aes(x = Species, y = Sepal.Length_mean)) +
  geom_segment(
    aes(x = Species, xend = Species, y = 0, yend = Sepal.Length_mean), 
    color = "lightgray"
    ) + 
  geom_point(aes(color = Species), size = 3) +
  scale_color_viridis_d() +
  theme_pubclean() +
  rotate_x_text(45)

```

```{r}
ggballoonplot(housetasks, fill = "value")+
  scale_fill_viridis_c(option = "C")

```



```{r}
#https://github.com/ricardo-bion/ggradar
library(ggradar);
library(scales);
suppressMessages(library(dplyr));
irisdata <- iris %>%
            group_by(Species) %>%
            mutate_each(funs(rescale)) %>%
            summarise( mean(Sepal.Length), mean(Sepal.Width), mean( Petal.Length ), mean( Petal.Width )  )

# ggradar(irisdata) + theme(text = element_text(size=20)) +ggradar(plot.data=df,
#                    font.radar = "Helvetica",
#                    grid.label.size = 4,
#                    axis.label.size = 4,
#                    plot.legend = T
#                    )

```


iris
iris6=iris2[,-1]
iris6=as.data.frame(iris6)
rownames(iris6)=iris2$Species
ggballoonplot(iris6, fill = "value")+
  scale_fill_viridis_c(option = "C")


           