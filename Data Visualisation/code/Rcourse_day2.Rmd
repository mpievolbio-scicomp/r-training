---
title: "Rcourse_day2"
author: "Loukas Theodosiou, PhD"
date: "5/3/2020"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
```


## Importing the data 

```{r}
#list.files() #to see what files I have in my working directory

library(readr) #you can read .csv and .txt files (csv=comma separated values, txt=values are separated by tab)

file=read_csv("mpg.csv")
file=read_tsv("mpg.txt")
help("read.table")
read.table("mtcars.txt", sep="\t", header=TRUE)


library(xlsx)
read.xlsx("mtcars2.xlsx",sheetIndex = 1)

library(readxl)
read_excel("mtcars2.xlsx",sheet = 1)
```

## Exporting data 

```{r}
write.csv(mpg,"/Users/theodosiou/Library/Mobile Documents/com~apple~CloudDocs/Documents/R_files/R EXERCISES/mtcars.csv") 

write.csv2(mpg,"/Users/theodosiou/Library/Mobile Documents/com~apple~CloudDocs/Documents/R_files/R EXERCISES/mtcars2.csv")

# !!! write_csv() uses “.” for the decimal point and a comma (“,”) for the separator. 
# !!! write_csv2() uses a comma (“,”) for the decimal point and a semicolon (“;”) for the separator. 

write.table(mtcars, file = "mtcars.txt", sep = "\t",row.names = TRUE, col.names = TRUE)

# !!! write_table() uses “.” for the decimal point and a \tab  for the separator.

write.xlsx(mpg, "/Users/theodosiou/Library/Mobile Documents/com~apple~CloudDocs/Documents/R_files/R EXERCISES/mtcars2.xlsx", sheetName = "Sheet1", col.names = TRUE, row.names = TRUE, append = FALSE)

#write xlsx2() is faster than write xlsx when it has to do for bigger files

```

### With R there is a possibility to read other types of files like rds,the unique data format of R, with readRDS as well as files from statistical programs like stata, spss, sas [source: https://rkabacoff.github.io/datavis/DataPrep.html]


## Lets read our data and try to plot it slowly
### first example is going to be nobel-prize winners

```{r}
data=read.csv("nobel_prize.csv", header = TRUE, na.strings=c("","NA"))
#lets explore the data
head(data)
str(data)
```
```{r}
#what is the most frequent of birth countries of the Nobel prize
library(dplyr)

data %>% group_by(Birth.Country) %>%count(sort=TRUE) #we always group_by a categorical variable

data %>% group_by(Sex) %>% count()

#filter NAs aka remove them
data %>% 
  filter(!is.na(Birth.Country),!is.na(Sex)) 
  group_by(Birth.Country) %>%
  count(sort=TRUE)
  
#filter only the female candidates
f=data %>% filter(Sex=="Female")
head(f)

#group the female winners into categories 
c=f %>% group_by(Category)
head(c)

cat=c %>% 
  select(Year, Full.Name, Category, Birth.Country) %>% 
  top_n(desc(Year))
head(cat)

#find which female won more than one nobel prize
prizewon=data %>% group_by(Full.Name) %>%  
  summarise(Prize_Count=n()) %>% 
  arrange(desc(Prize_Count))

#who won the noble prize more than 2 times
prizewon %>%  filter(Prize_Count>2) 

#what is the distribution of males and females across the different categories

sex=data %>% group_by(Category,Sex) %>% 
  summarise(c=n())

percent=sex %>% mutate(percent=c/sum(c))

ggplot(percent, aes(Category, percent, fill=Sex)) + geom_bar(stat="identity")+
  scale_fill_manual(values=, name=, labels=)

```

http://www.sthda.com/english/articles/24-ggpubr-publication-ready-plots/

https://color.adobe.com/explore
iris %>%  group_by(Species) %>% summarise()
https://dplyr.tidyverse.org/reference/summarise_all.html #really important for the summarise
https://www.r-graph-gallery.com/
http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html

