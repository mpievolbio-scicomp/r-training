---
title: "Day1_dplyr_Advanced"
author: "Loukas Theodosiou"
date: "5/29/2021"
output: html_document
---

- Why dplyr?
- It is an amazing package, for cleaning dirty datasets and bring them in the right 
format for ggplot

- What is structure of dplyr?
- dplyr has several function and you can chain them with pipe operator %>% 

- Do you want to me to show how to add this operator fast?
- Yes? Ok. Then go to Tools --> Modify Keyboard Shortcuts ---> Insert pipe Operator

```{r}
library(tidyverse)
library(palmerpenguins) # it contains the penguin data

penguins # have a look at the data; There are 8 variables included 
glimpse(penguins)
# Each variable is a column and its observation is a row

# species:        a factor denoting the penguin species (Adelie, Chinstrap, or Gentoo)
# island:         a factor denoting the island (in Palmer Archipelago, Antarctica) where observed
# bill_length_mm: a number denoting length of the dorsal ridge of penguin bill (millimeters)
# bill_depth_mm:  a number denoting the depth of the penguin bill (millimeters)
# flipper_length_mm: an integer denoting penguin flipper length (millimeters)
# body_mass_g:    an integer denoting penguin body mass (grams)
# sex:            a factor denoting penguin sex (male, female)
# year:           an integer denoting the study year (2007, 2008, or 2009)

```

Top20 functions in dplyr with examples

Top1 : filter()
Top2 : select()
Top3 : relocate()
Top4 : rename()
Top5 : mutate()
Top6 : group_by and summarise
Top7 : across()
Top8 : count()
Top9 : pivot_longer & pivot_wider
Top10: regular expressions


# Have a look on how the data looks like 
```{r}
ggplot(penguins, aes(x = island, fill = species)) +
  geom_bar(alpha = 0.8) +
  scale_fill_manual(values = c("darkorange","purple","cyan4"),
                    guide = FALSE) +
  theme_minimal() +
  facet_wrap(~species, ncol = 1) +
  coord_flip()

```
```{r}
ggplot(penguins, aes(x = sex, fill = species)) +
  geom_bar(alpha = 0.8) +
  scale_fill_manual(values = c("darkorange","purple","cyan4"),
                    guide = FALSE) +
  theme_minimal() +
  facet_wrap(~species, ncol = 1) +
  coord_flip()
```
```{r}
penguins %>%
  select(species, body_mass_g, ends_with("_mm")) %>%
  GGally::ggpairs(aes(color = species)) +
  scale_colour_manual(values = c("darkorange","purple","cyan4")) +
  scale_fill_manual(values = c("darkorange","purple","cyan4")) +
  theme_bw()

```


### Filter() -- refers to observations
```{r}
#Example 1 - Keep only the species Chinstrap
penguins %>% 
  filter(species=="Chinstrap")

#Example 2 = Include Chinstrap and Gentoo
penguins %>% 
  filter(species =="Chinstrap" | species=="Gentoo")

#or you use the amazing include function
penguins %>% 
filter(species %in% c("Chinstrap","Gentoo"))

#Example3 = if you want to have both Chinstrap and Gentoo, no matter what then you "," or "&"

penguins %>% 
  filter(species =="Adelie", island=="Dream")

penguins %>% 
  filter(species =="Adelie" & island=="Dream")

# Example 4 = keep penguins with flipper length > 4mm
penguins %>%
  filter(flipper_length_mm > 200)

# or penguing with body mass less or equal to 2900g
penguins %>%
  filter(body_mass_g <= 2900)

# Example 5 = A small exercise: Please filter to keep species ="Adelie" and bill_length_mm>40
penguins %>%
  filter(species == "Adelie", bill_length_mm > 40)

# Example 6 = filter our hte species "Chinstrap"
penguins %>%
  filter(species != "Chinstrap")

# Exercise 7 = Create a subset from penguins containing observations for female chinstrap penguins on Dream and Torgersen Islands.

penguins %>% 
  filter(sex=="female",species=="Chinstrap") %>% 
  filter(island %in% c("Dream","Torgesen") )

```
### Select()  --- refers to variables

```{r}
# Example 1 - you select three columns 
penguins %>% 
  select(year, island, species)

# Example 2 - keep all columns from specie to body_mass_g
penguins %>% 
  select(species:body_mass_g)

# Example 3 - keep all column from species to bill_depth_mm and the year 
penguins %>% 
  select(species:bill_depth_mm, year)

# Example 4 - use the "-" to remove a variable 
penguins %>% 
  select(-island)

# Example 5 - keep everything except from the following columns
penguins %>% 
  select(!(island:bill_depth_mm))

# Example 6 - keep everything except from  species, flipper_length and year 
penguins %>%
  select(!c(species, flipper_length_mm, year))

# Example 7 - useful functions starts_with, ends_with and contains
# starts_with 
penguins %>% 
  select(starts_with("bill"))

# ends with
penguins %>% 
  select(ends_with("mm"))

# contains
penguins %>%
  select(contains("length"))

# Example 8 - please select everything that contains island or starts with bill
penguins %>%
  select(contains("island") | starts_with("bill"))

# Example 9 - Make a subset from penguins that only contains observations for male penguins with flippers longer than 200 mm, and then only keep columns that end with “mm"

penguins %>% 
  filter(sex="male", flipper_length_mm >200) %>% 
  select(ends_with("mm"))


```
### Relocate()
You can use .before or .after to move a column 
if you just type relocate(col_A), it will move this column first

```{r}
# Example1 --to put year as the first column
penguins %>% 
  relocate(year)

# Example2 -- relocate flipper_length_mm after island 
penguins %>% 
  relocate(flipper_length_mm, .after = island)

# Example3 -- relocate  .before
penguins %>% 
  relocate(sex, .before=island)

# Example 4 --- relocate all the numeric ones after the factors 

penguins %>% 
  relocate(where(is.numeric), .after=where(is.factor))

```
### Rename () --- variables

```{r}
# Example 1 --- rename one variable

penguins %>% 
  rename(Loukas_islands= island)

# Example 2 -- rename two variables 
glimpse(penguins)

penguins %>% 
  rename(islands=island, bill_length = bill_length_mm)

# Example 3 -- make all names with upper case letters 

penguins %>% 
  rename_with(toupper, ends_with("mm"))

```
### mutate 
- Why mutate?  use mutate to add a new column while keeping the existing ones. Imagine that I have 
a data frame (df) with columns A and B. I want to create a new column C which will be the sum 
of A and B. df %>% mutate(C=A+B)
```{r}
# Example 1 --- from g to Kg
penguins %>% 
  mutate(body_mass_kg = body_mass_g / 1000)

# Example 2 --- find a ratio 
penguins %>% 
  mutate(bill_ratio= bill_length_mm/bill_depth_mm)

#Example 3 --- mutate multiple columns 
penguins %>% 
  mutate(bill_ratio = bill_length_mm / bill_depth_mm,
         body_mass_kg = body_mass_g / 1000,
         flipper_length_m = flipper_length_mm / 1000)

#Example 4 --- add an id to each observation where n() indicates the number of observations or length of data frame

penguins %>% 
  mutate(id = seq(1:n()))


# Example 5 --- mutate a class from factor to character

penguins %>% 
  mutate(island = as.character(island))

# Example 6 - SOS relevel factors in a data.frame 

penguins %>% 
  mutate(island=fct_relevel(island, "Torgersen", "Biscoe", "Dream"))
levels(a$island)

# Example 7 - make a simple exercise 

# The year column in penguins is currently an integer. Add a new column named year_fct that is the year converted to a factor (hint: as.factor()).

# Example 8 --- case when this is one of my favorite functions 
penguins %>% 
  mutate(speciesL = case_when(species=="Adelie" ~ "Loukas"))

 penguins %>% 
  mutate(speciesL = case_when(species == "Adelie" ~ "adeliae", 
                              species == "Chinstrap" ~ "antarctica", 
                              species == "Gentoo" ~ "papua")) %>% 
  relocate(speciesL, .after = 1)


library("GGally")

penguins %>%
  select(species, body_mass_g, ends_with("_mm")) %>%
  GGally::ggpairs(aes(color = species))

# Make an exercise where you creat a column id and you put it before columns that contain the "_" 
dplyr::mutate(studygroupid = row_number(), .before = contains("_"))


```

### Summarise to prepare a summary table containing mean, sd and se , na.rm =TRUE is essential to remove Na values --- Observations

```{r}
# Example 1 - finding the mean 

penguins %>% 
  group_by(species) %>% 
  summarise(mass_mean = mean(body_mass_g, na.rm=TRUE))

# Example 2 - finding the sd

penguins %>% 
  group_by(species) %>% 
  summarise(mass_mean = mean(body_mass_g, na.rm=TRUE), 
            mass_sd = sd(body_mass_g, na.rm = TRUE))


# Example 3  - summarise using sex and species 

penguins %>%
  group_by(species, sex) %>%
  summarize(bill_length_mean = mean(bill_length_mm, na.rm = TRUE),
            bill_length_sd = sd(bill_length_mm, na.rm = TRUE))


```

### summarise(across())  is great for summarising multiple variable with one or more functions 

```{r}
# Example 1 - Create a summary table for all variables which end in mm 
penguins %>% 
  group_by(species) %>% 
  summarise(bill_length_mean = mean(bill_length_mm, na.rm = TRUE),
            bill_depth_mean = mean(bill_depth_mm, na.rm = TRUE),
            flipper_length_mean = mean(flipper_length_mm, na.rm = TRUE))


# Example 2 - How to summarise exactly two specific two columns in R 

penguins %>% 
  group_by(species) %>% 
  summarise(across(c(bill_depth_mm, body_mass_g), list(mean=mean, sd=sd), na.rm=TRUE))

# Example 3 - Summarise the one that ends with "mm:
penguins %>% 
  group_by(species) %>% 
  summarise(across(ends_with("mm"), list(mean=mean,sd=sd), na.rm=TRUE))

# Example 4
# one of the interesting things in the summarise is the names output to control the output_names
# in this case the mean is coming to the end. How to bring it first
penguins %>% 
  group_by(species) %>% 
  summarise(across(where(is.numeric), list(mean=mean, sd=sd),na.rm=TRUE))
  
penguins %>% 
  group_by(species) %>% 
  summarise(across(where(is.numeric), list(mean=mean, sd=sd),.names="{.fn}_{.col}",na.rm=TRUE))

penguins %>% 
  group_by(species) %>% 
  summarise(across(where(is.numeric), list(mean=mean, sd=sd), .names="{.col}_{.fn}",na.rm=TRUE))

```


### Count -- observation, with the count you actually count the observations
you can use either 
Using group_by() %>% summarize() with n() to count observations
Using count() to do the exact same thing
```{r}
 penguins %>% 
  group_by(species) %>% 
  summarise(n())
 #or 

 penguins %>% 
count(species)

 penguins %>%
  count(species, year,island)
 
 penguins %>%
  count(island)
 
 penguins
 
```
### Pivot longer and pivot wider 
use extra functions from here https://dcl-wrangle.stanford.edu/pivot_advanced.html # advanced pivoting


```{r}
# Example 1 - simple use
penguins %>% 
  pivot_longer(!c(species,island,sex), names_to="variable", values_to="value")

# also use the where...
penguins %>% 
  pivot_longer(where(is.numeric), names_to="variable", values_to = "value")

# also use the cols=starts_with

penguins %>% 
  pivot_longer(cols=starts_with("bill"), names_to="bill", values_to = "value",  values_drop_na = TRUE)

penguins %>% 
  pivot_longer(cols=c(bill_length_mm,flipper_length_mm), names_to="length", values_to = "value",  values_drop_na = TRUE)

# example with the the names prefix and name types

penguins %>% 
  pivot_longer(cols = c(bill_length_mm,flipper_length_mm), names_to = "length", names_prefix = "bill_", names_ptypes = list(length=factor()))

# it can go a lot more complicated but for now focus on this
penguins %>% 
  pivot_longer(contains("_"), 
               names_to = c("part", "measure", "unit"), 
               names_sep = "_")
  
            
```
### pivot wider ---- variables 

```{r}

penguins %>% 
  pivot_wider(names_from = "island", values_from = "bill_depth_mm")

```
### Summary of pivot_longer and pivot_wider 

both have similar function 
pivot_longer = names_to [refers to variables that will become one column]
pivot_wider = names_from [refers to variables that will become multiple columns]

pivot_longer = values_to [refers to the name of all these observations that ll be grouped in one column]
pivot_longer = values_from [now that you created multiple columns fill them with values ]

# this is also for doing some plots 
https://allisonhorst.github.io/palmerpenguins/articles/intro.html

# this is interesting how to glue names in the variables in R 
https://rdrr.io/github/tidyverse/tidyr/man/pivot_wider.html

# this is another interesting link for cleaning data functions
https://towardsdatascience.com/the-essential-dplyr-cdf3057c1c6c

# cool illustrations to understand dplyr 
https://swcarpentry.github.io/r-novice-gapminder/13-dplyr/
# this one is cool as well
https://ohi-science.org/data-science-training/dplyr.html

# my main source for the course 
https://allisonhorst.shinyapps.io/dplyr-learnr/#section-dplyrcount

# keep this for new packages 
https://osf.io/q8esd/

# this is a cool command
## install and call required packages
pkgs <- c("taxize", "assertr", "stringdist", 
          "tidyverse", "palmerpenguins", "GGally")
# lapply(pkgs, install.packages, character.only = TRUE)
lapply(pkgs, library, character.only = TRUE)

# I need this for regular expressions 

https://bookdown.org/rdpeng/rprogdatascience/regular-expressions.html#the-stringr-package


# run statistics and a model in R 
https://towardsdatascience.com/ten-up-to-date-ways-to-do-common-data-tasks-in-r-4f15e56c92d

# Top 10 -- regular expressions -- observations but characters

# regular expression tend to be scary - boring - not fun 
https://twitter.com/daattali/status/1398032624933871616

### searching
?grep ## index or values
?grepl ## true/false

## string manipulation
?sub ## replaces first instance
?gsub ## replaces all instances

## string extraction
?substr ## specific locations 

## string splitting on character
?strsplit #separating chars


## regex keywords

## ^ beginning of a string
## $ end of a string 
## \\d digits
## \\s white spaces
## \\w alphanumeric words
## . anything

## * match 0 - infinity times
## + match 1 to infinity times
## ? match 0-1 times

## [] match inside these brackets "exactly"
##    [A-Za-z] upper and lower case letters
##    [0-9] all numbers between 0-9
##    [Ss]hot (example- shot with caps or not)

## () used to identify groupings for gsub work


I think in here the explanations are a bit better
https://stringr.tidyverse.org/articles/regular-expressions.html

```{r}

# Example 1 -- Essential on how to replc
penguins %>% 
  mutate(species2=str_replace_all(species, "Adelie", "Adelaida")) 

#or
# but in here you do not keep the Gentoo
penguins %>% 
  mutate(species2 =case_when(species=="Adelie"~"Adelaids",species=="Chinstrap"~"Chin"))

#remotes::install_github("decisionpatterns/stringr.tools")
library(stringr.tools)

# add a prefix in the strings of a column 
penguins %>% 
  mutate(species2 = str_prefix(species,"penguin", "pre."))

# and this is how you can add it in a traditional way 
penguins2 <- penguins %>%
   mutate(species2 = case_when(species == "Adelie" ~ paste0("Adelie-", "penguins")))
penguins2

# and how you can split the columns based on a character

rm(list = ls())
str(penguins2)
 
separate(data=penguins2, col=species2, into=c("y1","y2"), sep="-", fill = "right")


# here you paste a prefix in all columns names
dplyr::select_all(mtcars, .funs = funs(paste0("cars.", .)))


```
col1 <- c("test-1", "test-2",test)
col2 <- c(1,2,3)


### tidy models 
https://jimgruman.netlify.app/post/variableimportance/

```{r}
#_____________________________________
#   ,---. ,--,--.,--.--. ,---.  
#  | .--'' ,-.  ||  .--'(  .-'  
#  \ `--.\ '-'  ||  |   .-'  `) 
#   `---' `--`--'`--'   `----' 
#______________________________________________
rm(list=ls())

# Libraries and Fonts

library(tidyverse)  # the best package
library(here)       # you can use here to modify your working directory
library(janitor)    # cleans dirty data 
library(remotes)
library(jkmisc)     # it contains functions that kaup wrote
library(ggalt)      # new geometries and scales in R
library(ggtext)     # adding text
library(glue)       # you glue strings
library(ragg)       # written by Thomas Lind Petersen and allows to produce images of higher quality
library(showtext)

font_paths() 
# @ load the fonts you need 
font_add("Avenir_Next",regular= "/System/Library/Fonts/Avenir Next.ttc")  # Use the actual file path
font_add(family = "Montserrat",  regular = "/Users/theodosiou/OneDrive/Affinity_designer/FONTS/Montserrat-Regular.ttf")
font_add_google("Oswald", "Oswald")
font_add_google("Lora", "Lora")
font_add_google("Lato", "Lato")
showtext_auto()  # you need to run the showtext auto to make sure that the fonts are available

# @ data.frame

cars <- readr::read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-10-15/big_epa_cars.csv")

glimpse(cars)   # here you get an idea of your data frame
problems(cars)  # here it tells you what problems might be in the data.frame. I am not sure yet how it works but I hope to find soon


# @ Here I am preparing a data set where I check how many brands have been sold/manufactured 37 times

top <- cars %>% 
  clean_names() %>%        # this is a janitor fucntion, it is very cool since it cleans the names in a very nice way: look example in here https://cran.r-project.org/web/packages/janitor/vignettes/janitor.html
  count(make, year) %>%    # you count the make per year
  count(make) %>%          # you aslo count the make by itself
  filter(n == 37)           # I am not sure yet what is this, I guess that it refers to counts with n==37


plot_data <- cars %>% 
  select(make, VClass, year, youSaveSpend) %>% 
  semi_join(top) %>% 
  group_by(year, make) %>% 
  summarize(total_save_spend = mean(youSaveSpend)) %>%
  group_by(year) %>% 
  mutate(rank = min_rank(desc(total_save_spend))) %>%   # so here it is cool since it ranks your data based on the value: total_save_spend. So actually it ranks all cars and years based on the money that have spent
  ungroup() %>% 
  mutate(size = if_else(make == "Ford", 1, 0.5),   # and here I guess is the coolest part and then I can go for work
         make = factor(make, pull(top, make)),
         make = fct_relevel(make, "Ford", after = Inf),
         make = fct_recode(make, `**Ford**` = "Ford"))

grid <- tibble(rank = 1:22)

colors <- set_names(grey.colors(22), pull(top, make))

colors[["Ford"]] <- "#DD2A7B"


plot <- ggplot(plot_data, aes(x = year, y = rank)) +
  geom_segment(data = grid, aes(x = 1983, xend = 2021, y = rank, yend = rank), color = "#cccccc", alpha = 0.5, size = 0.1) +
  geom_xspline(aes(color = make, size = size), show.legend = FALSE) +
  geom_point(aes(fill = make), shape = 21, color = "white", show.legend = FALSE) +
  geom_richtext(data = filter(plot_data, year == 2020), aes(label = make, x = 2021, color = make), hjust = 0, family = "Oswald", size = 4, show.legend = FALSE,  fill = NA, label.color = NA, 
                label.padding = grid::unit(rep(0, 4), "pt")) +
  geom_text(data = filter(plot_data, year == 1984), aes(label = rank, x = 1983), hjust = 1, family = "Oswald", size = 4) +
  labs(x = NULL,
       y = NULL,
       title = "From Chugging to Sipping: Fuel Cost Savings of Major Automakers since 1984",
       subtitle = glue("Shown below is a rankings chart of average fuel cost savings, measured over 5 years, from 1984 to 2020.  {highlight_text('Ford','#DD2A7B', 'b')} has had quite the journey, battling from the bottom<br>of the list to the second-best North American manufacturer.")) +
  scale_color_manual(values = colors) +
  scale_fill_manual(values = colors) +
  scale_size_identity() +
  scale_x_continuous(breaks = 1984:2020) +
  scale_y_continuous(trans = "reverse", breaks = NULL) +
  expand_limits(x = 2025) +
  theme_jk(grid = "X", 
           markdown = TRUE)

plot

ggsave(here("2019", "week42", "tw42_plot.png"), plot = plot, width = 13, height = 6)

```



