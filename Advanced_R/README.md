# Advanced R - 3 day course

This course covers advanced aspects and use cases of the R programming language.

We therefore recommend it to students who (will) have completed the "R for Beginners" workshop and are familiar with

- Using the R-studio IDE
- Version control with git
- Loading data from external files or web resources
- Exploratory data analysis with R
- Plotting with ggplot2
- Scripting with R
- Flow control in R
- Loops in R
- Defining a function in R

# RStudio Server

Please connect to the following link to work with RStudio and R:

https://rstudio.gwdg.de/

# Timetable

## Day1 - Data Wrangling

| Time  | Title | Lecturer | Course material |
| ----- | ----- | -------- | --------------- |
| 10:00 | Presentation of the example dataset | Loukas Theodosiou | |
| 11:00 | The tidyverse package part 1 | Loukas Theodosiou | |
| 12:00 | Lunch break | | |
| 13:00 | The tidyverse package part 2 | Loukas Theodosiou | |
| 14:00 | Welcome and Introduction | Loukas Theodosiou | |
| 16:00 | Discussion of Solutions | All | |

## Day2 - Multiprocessing

| Time  | Title | Lecturer | Course material |
| ----- | ----- | -------- | --------------- |
| 10:00 | Making R fast | Carsten Fortmann-Grote | |
| 11:00 | Big Data in R | Carsten Fortmann-Grote | |
| 12:00 | Lunch break | | |
| 13:00 | Deep Learning with R | Carsten Fortmann-Grote | |
| 14:00 | Making R fast | Carsten Fortmann-Grote | |
| 16:00 | Discussion of Solutions | All | |

## Day3 - Genetics

| Time  | Title | Lecturer | Course material |
| ----- | ----- | -------- | --------------- |
| 10:00 | Sequences as strings | Kristian Ullrich | |
| 11:00 | Sequences comparisons | Kristian Ullrich | |
| 12:00 | Lunch break | | |
| 13:00 | VCF processing in R | Kristian Ullrich | |
| 14:30 | Tree visualization | Kristian Ullrich | |
| 15:00 | Discussion of Solutions | All | |

## RStudio Cheatsheets

https://www.rstudio.com/resources/cheatsheets/

[base-r](http://github.com/rstudio/cheatsheets/raw/master/base-r.pdf)

[advancedR](https://www.rstudio.com/wp-content/uploads/2016/02/advancedR.pdf)

[Rmarkdown](https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf)

[Rmarkdown reference Guide](https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf)

[stringr](https://github.com/rstudio/cheatsheets/raw/master/strings.pdf)

[purrr](https://github.com/rstudio/cheatsheets/raw/master/purrr.pdf)

[dplyr](https://github.com/rstudio/cheatsheets/raw/master/data-transformation.pdf)

[ggplot2](https://github.com/rstudio/cheatsheets/raw/master/data-visualization-2.1.pdf)

[Regular Expressions](https://github.com/rstudio/cheatsheets/raw/master/regex.pdf)

[Parallel Computation](https://github.com/rstudio/cheatsheets/raw/master/parallel_computation.pdf)

[keras](https://github.com/rstudio/cheatsheets/raw/master/keras.pdf)

## other Links

[Biostrings](https://www.bioconductor.org/packages/release/bioc/html/Biostrings.html)

[ape](http://ape-package.ird.fr/)

[seqinr](http://seqinr.r-forge.r-project.org/)

[coRdon](http://www.bioconductor.org/packages/release/bioc/html/coRdon.html)

[CRBHits](https://gitlab.gwdg.de/mpievolbio-it/crbhits)

[PopGenome](https://cran.r-project.org/web/packages/PopGenome/vignettes/An_introduction_to_the_PopGenome_package.pdf)

[vcfR](https://cran.r-project.org/web/packages/vcfR/vignettes/intro_to_vcfR.html)
