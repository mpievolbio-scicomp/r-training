---
title: "Tree Visualization"
author: "Kristian Ullrich"
date: "4/6/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## load libraries for this part

```{r}
library(ggplot2)
library(cowplot)
library(ggridges)
library(tidyverse)
library(tidytree)
library(ape)
library(seqinr)
library(phangorn)
library(phytools)
library(treeio)
library(ggtree)
library(phyloseq)
library(Biostrings)
library(CRBHits)
library(distSTRING)
```

## Get distances

```{r}
data(hiv, package = "distSTRING")
# K80 distance
hiv.K80 <- distSTRING::dnastring2dist(hiv, model = "K80")
hiv.K80.dist <- as.dist(hiv.K80$distSTRING)
# NG86 distance
hiv.NG86 <- CRBHits::dnastring2kaks(hiv, model = "NG86")
# get distance matrix
hiv.NG86.dist <- cbind(rbind(NA, reshape2::dcast(hiv.NG86, seq2 ~ seq1, value.var = "pn")), NA)
hiv.NG86.dist[1, 1] <- colnames(hiv.NG86.dist)[2]
rownames(hiv.NG86.dist) <- hiv.NG86.dist[, 1]
hiv.NG86.dist <- hiv.NG86.dist[, -1]
colnames(hiv.NG86.dist) <- rownames(hiv.NG86.dist)
hiv.NG86.dist <- as.dist(hiv.NG86.dist)
```

## Neighbor joining tree calculation with `bionj` function from `ape`

```{r}
# K80 distance
hiv.K80.dist.bionj <- ape::bionj(hiv.K80.dist)
str(hiv.K80.dist.bionj)
plot(hiv.K80.dist.bionj)
# NG86 distance
hiv.NG86.dist.bionj <- ape::bionj(hiv.NG86.dist)
plot(hiv.NG86.dist.bionj)
```

## Cophyloplot

```{r}
association <- cbind(hiv.K80.dist.bionj$tip.label, hiv.K80.dist.bionj$tip.label)
ape::cophyloplot(hiv.K80.dist.bionj, hiv.NG86.dist.bionj, assoc = association,
            length.line = 4, space = 28, gap = 3)
# with rotation
ape::cophyloplot(hiv.K80.dist.bionj, hiv.NG86.dist.bionj, assoc = association,
            length.line = 4, space = 28, gap = 3, rotate = FALSE)
#ape::cophyloplot(hiv.K80.dist.bionj, hiv.NG86.dist.bionj, assoc = association,
#            length.line = 4, space = 28, gap = 3, rotate = TRUE)
```

## NeighborNet - `phangorn` package

```{r}
hiv.NK80.dist.nnet <- phangorn::neighborNet(hiv.K80.dist)
hiv.NG86.dist.nnet <- phangorn::neighborNet(hiv.NG86.dist)
plot(hiv.NK80.dist.nnet)
plot(hiv.NG86.dist.nnet)
```

## Estimating ML tree

<https://cran.r-project.org/web/packages/phangorn/vignettes/Trees.html>

```{r}
dat <- phangorn::as.phyDat(distSTRING::aastring2aln(distSTRING::cds2aa(hiv)), type = "AA")
dm <- dist.ml(dat, model="JTT")
tree <- NJ(dm)
plot(tree)
# parallel will only work safely from command line
# and not at all windows
(mt <- modelTest(dat, model=c("JTT", "LG", "WAG"),
    multicore=TRUE))
# run all available amino acid models
(mt <- modelTest(dat, model="all", multicore=TRUE))

env <- attr(mt, "env")
fitStart <- eval(get(mt$Model[which.min(mt$BIC)], env), env)

fitNJ <- pml(tree, dat, model="JTT", k=4, inv=.2)
fit <- optim.pml(fitNJ, rearrangement = "stochastic",
    optInv=TRUE, optGamma=TRUE)
fit

bs <- bootstrap.pml(fit, bs=100, optNni=TRUE, multicore=TRUE)
# plot mid-point rooted tree also showing bootstraps values higher than 50%
plotBS(midpoint(fit$tree), bs, p = 50, type="p")
cnet <- consensusNet(bs, p=0.2)
plot(cnet, show.edge.label=TRUE)
```

## Cophyloplot with `phangorn` and `phytools`

<https://cran.r-project.org/web/packages/phangorn/vignettes/IntertwiningTreesAndNetworks.html>

```{r}
par(mfrow=c(2,1))
obj <- cophylo(hiv.K80.dist.bionj, hiv.NG86.dist.bionj)
plot(obj, scale.bar=c(.05,.05), ylim=c(-.2,1))
```

## Phylogentic Tree Visualization with the `treeio` package

<https://yulab-smu.top/treedata-book/index.html>

```{r}
data(woodmouse, package = "ape")
d <- ape::dist.dna(woodmouse)
tr <- ape::nj(d)
bp <- ape::boot.phylo(tr, woodmouse, function(x) ape::nj(ape::dist.dna(x)))
fun <- function(x) ape::bionj(ape::dist.dna(x))
bstrees <- ape::boot.phylo(tr, woodmouse, fun, trees = TRUE, B = 100)$trees
p1 <- ggtree(tr)
p2 <- ggtree(tr) + geom_tiplab()
p3 <- ggtree(tr, layout="ellipse") + geom_tiplab()
p4 <- ggtree(tr, layout="circular") + geom_tiplab()
cowplot::plot_grid(p1, p2, p3, p4)
# densitree
ggdensitree(bstrees, alpha=.3, colour='steelblue') + geom_tiplab(size=3)
```

## Plotting Tree with Data

```{r}
# color nodes by GC content
dnastring2gccontent <- function(dna, global = TRUE){
  dna_aF <- as.data.frame(alphabetFrequency(dna))
  if(global){
    dna_aF <- as.data.frame(t(as.matrix(colSums(dna_aF))))
  }
  dna_GC <- dna_aF %>% summarise(GC = (G+C) / (A+G+C+T))
  return(dna_GC)
}
data(woodmouse, package = "ape")
woodmouse.dnastring <- distSTRING::dnabin2dnastring(woodmouse)
woodmouse.dnastring.gc <- dnastring2gccontent(woodmouse.dnastring, global = FALSE)
woodmouse.dnastring.gc <- cbind(label = names(woodmouse.dnastring), woodmouse.dnastring.gc)
# left_join p3 and GC
pGC <- ggtree(tr) + geom_tiplab()
pGC$data <- pGC$data %>% left_join(woodmouse.dnastring.gc)
pGC + geom_point(aes(fill = GC), shape = 21, size = 4)
pGC + geom_point(aes(fill = GC), shape = 21, size = 4) + geom_hilight(mapping=aes(subset = node %in% c(5, 12)))
pGC + geom_point(aes(fill = GC), shape = 21, size = 4) + geom_hilight(mapping=aes(subset = isTip, fill = GC))
# write fasta file
writeXStringSet(distSTRING::dnabin2dnastring(woodmouse), file = "data/woodmouse.fasta")
# plot tree with MSA
p <- ggtree(tr) + geom_tiplab(size=3)
msaplot(p, "data/woodmouse.fasta", width = 1)
```

## Advanced Tree Visualization

<https://yulab-smu.top/treedata-book/chapter8.html>
<https://yulab-smu.top/treedata-book/chapter9.html>
<https://yulab-smu.top/treedata-book/chapter10.html>

```{r}
data(GlobalPatterns, package = "phyloseq")
GP <- GlobalPatterns
GP <- prune_taxa(taxa_sums(GP) > 600, GP)
sample_data(GP)$human <- get_variable(GP, "SampleType") %in% 
  c("Feces", "Skin") 

mergedGP <- merge_samples(GP, "SampleType")
mergedGP <- rarefy_even_depth(mergedGP,rngseed=394582)
mergedGP <- tax_glom(mergedGP,"Order") 

melt_simple <- psmelt(mergedGP) %>% 
  filter(Abundance < 120) %>% 
  select(OTU, val=Abundance)

ggtree(mergedGP) + 
  geom_tippoint(aes(color=Phylum), size=1.5) +
  geom_facet(mapping = aes(x=val,group=label, 
                           fill=Phylum),
            data = melt_simple, 
            geom = geom_density_ridges,
            panel="Abundance",  
            color='grey80', lwd=.3)
```

## Gene Feature Plotting

<https://wilkox.org/gggenes/>

__TASK:__
