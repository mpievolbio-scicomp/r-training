#! /bin/bash
# SBATCH -p medium
# SBATCH -t 00:02:00
# SBATCH -N 1
# SBATCH -c 20
# SBATCH -o job-%J.out
# SBATCH -e job-%J.err
# SBATCH --mail-type=ALL
# SBATCH --mail-user=grotec@evolbio.mpg.de

module purge
module load openmpi r/4.0.3

mpirun Rscript mdplyr.R
